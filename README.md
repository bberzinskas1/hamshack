# Ansible stuff to setup my hamlab
a lot of this started with a pi and this site..   then i had more better computers to load some of it on

An `ansible` playbook runs a series of ansible roles, depending on what i want to do from a host.   
I created 3 hosts, "desktop", "sat/weather", and "packet".


# "architecture"
AKA, so i don't forget

### SDR Audio
RTL-SDR streams to audio..   Pulseaudio intercepts it from `alsa` and allows us to route it elsewhere.
-------------------     -------------------
|   RTL-SDR app   |  >  |   Pulse audio   |
-------------------     -------------------


## SDR Tuning
rigctl exposes a port and allows us to tune the sdr across the network..

-------------------    -------------------
| rigctl / hamlib | >  |    cubicsdr     |
-------------------    -------------------


# ref
https://dl1gkk.com/setup-raspberry-pi-for-ham-radio/
https://www.radiosrs.net/digital_modes.html
https://osmocom.org/projects/rtl-sdr/wiki/Rtl-sdr
