#FT991a UART Interfaces
SUBSYSTEM=="tty", GROUP="dialout", MODE="0660", DRIVERS=="cp210x", ATTRS{interface}=="Enhanced*", SYMLINK+="yaesu991e"
SUBSYSTEM=="tty", GROUP="dialout", MODE="0660", DRIVERS=="cp210x", ATTRS{interface}=="Standard*", SYMLINK+="yaesu991s"
