[Unit]
Description=rigctld Hamradio rig controler
After=syslog.target network.target
[Service]
Type=simple
ExecStart=/usr/bin/rigctld -v -m 1035 -r /dev/yaesu991e

ExecReload=/bin/kill -HUP $MAINPID
RestartSec=60
Restart=always
User=rigctld
Group=rigctld
[Install]
WantedBy=multi-user.target
