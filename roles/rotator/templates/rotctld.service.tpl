[Unit]
Description=rotctld Hamradio antenna rotator controller
After=syslog.target network.target
[Service]
Type=simple
ExecStart=/usr/bin/rotctld -m 202 -r /dev/ttyUSB0 -s 9600 -v

ExecReload=/bin/kill -HUP $MAINPID
RestartSec=60
Restart=always
User=rotctld
Group=rotctld

[Install]
WantedBy=multi-user.target
