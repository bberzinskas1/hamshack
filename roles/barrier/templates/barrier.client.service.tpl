[Unit]
Description=Barrier mouse/keyboard client
After=network.target

[Service]
ExecStart=/usr/bin/barrierc -f -l /var/log/barrier --no-daemon --no-restart --disable-crypto -n {{ hostvars[inventory_hostname].group_names[0] }} {{ hostvars['nuc1']['ansible_default_ipv4']['address'] }}
Restart=always
RestartSec=10
User=bblaze

[Install]
WantedBy=multi-user.target
