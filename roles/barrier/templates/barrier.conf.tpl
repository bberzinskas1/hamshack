#  barrier configuration file
#
# +-------+  +--------+ +---------+
# |XX     |  |Desktop | |SAT      |
# |       |  |        | |Packet   |
# +-------+  +        + +---------+
# +-------+  +        + +---------+
# |XX     |  |        | |SAT      |
# |       |  |        | |Packet   |
# +-------+  +--------+ +---------+

section: screens
	node5:
	node4:
	desktop:
	sat:
	packet:
end

section: links
	desktop:
		left  = node4
		right = packet

	sat:
		left = desktop
		down = packet

	packet:
		left = desktop
		up = sat

	node5:
		right = desktop
		down = node4

	node4:
		right = desktop
		up = node5

end
