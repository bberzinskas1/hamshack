[Unit]
Description=Barrier mouse/keyboard server
After=network.target

[Service]
ExecStart=/usr/bin/barriers -a 0.0.0.0 -f --no-daemon --no-restart --config /etc/barrier.conf -l /var/log/barrier --disable-crypto -n {{ hostvars[inventory_hostname].group_names[0] }}
Restart=always
RestartSec=10
User=bblaze


[Install]
WantedBy=multi-user.target
